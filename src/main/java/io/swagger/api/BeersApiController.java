package io.swagger.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.ApiParam;
import io.swagger.model.BeerItem;
import io.swagger.service.BeersService;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-08-14T00:03:04.071Z[GMT]")
@Controller
public class BeersApiController implements BeersApi {

	@Autowired
	BeersService sv;

	public ResponseEntity<Void> addBeers(@ApiParam(value = "Ingresa una nueva cerveza") @Valid @RequestBody BeerItem body) {
		return sv.addBeers(body);
	}

	public ResponseEntity<BeerItem> searchBeerById(@ApiParam(value = "Busca una cerveza por su Id", required = true) @PathVariable("beerID") Integer beerID) {
		return sv.searchBeerById(beerID);
	}

	public ResponseEntity<List<BeerItem>> searchBeers() {
		return sv.searchBeers();
	}

}
