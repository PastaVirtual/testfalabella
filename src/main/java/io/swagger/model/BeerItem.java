package io.swagger.model;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * BeerItem
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-08-14T00:03:04.071Z[GMT]")
@Entity
@Table(name = "beer_item")
public class BeerItem {
	@JsonProperty("Id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@JsonProperty("Name")
	@Column(name = "name")
	private String name;

	@JsonProperty("Brewery")
	@Column(name = "brewery")
	private String brewery;

	@JsonProperty("Country")
	@Column(name = "country")
	private String country;

	@JsonProperty("Price")
	@Column(name = "price")
	private BigDecimal price;

	public BeerItem id(Integer id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(example = "1", required = true, value = "")
	@NotNull

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BeerItem name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	@ApiModelProperty(example = "Golden", required = true, value = "")
	@NotNull

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BeerItem brewery(String brewery) {
		this.brewery = brewery;
		return this;
	}

	/**
	 * Get brewery
	 * 
	 * @return brewery
	 **/
	@ApiModelProperty(example = "Kross", required = true, value = "")
	@NotNull

	public String getBrewery() {
		return brewery;
	}

	public void setBrewery(String brewery) {
		this.brewery = brewery;
	}

	public BeerItem country(String country) {
		this.country = country;
		return this;
	}

	/**
	 * Get country
	 * 
	 * @return country
	 **/
	@ApiModelProperty(example = "Chile", required = true, value = "")
	@NotNull

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public BeerItem price(BigDecimal price) {
		this.price = price;
		return this;
	}

	/**
	 * Get price
	 * 
	 * @return price
	 **/
	@ApiModelProperty(example = "10.5", required = true, value = "")
	@NotNull

	@Valid
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BeerItem beerItem = (BeerItem) o;
		return Objects.equals(this.id, beerItem.id) && Objects.equals(this.name, beerItem.name) && Objects.equals(this.brewery, beerItem.brewery) && Objects.equals(this.country, beerItem.country) && Objects.equals(this.price, beerItem.price);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, brewery, country, price);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BeerItem {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    brewery: ").append(toIndentedString(brewery)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    price: ").append(toIndentedString(price)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public BeerItem(Integer id, String name, String brewery, String country, BigDecimal price) {
		this.id = id;
		this.name = name;
		this.brewery = brewery;
		this.country = country;
		this.price = price;
	}

	public BeerItem() {
	}

}
