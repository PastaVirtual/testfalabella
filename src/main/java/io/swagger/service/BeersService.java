package io.swagger.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.swagger.api.BeersApi;
import io.swagger.dao.BeerItemRepository;
import io.swagger.model.BeerItem;

@Service
public class BeersService implements BeersApi {

	@Autowired
	BeerItemRepository repo;

	private final Logger logger = LoggerFactory.getLogger(BeersService.class);

	@Override
	public ResponseEntity<Void> addBeers(BeerItem body) {
		try {
			repo.save(body);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<BeerItem> searchBeerById(Integer id) {
		try {
			BeerItem beer = repo.findById(id).get();
			new ResponseEntity<BeerItem>(beer, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<List<BeerItem>> searchBeers() {
		try {
			List<BeerItem> listBeer = new ArrayList<>();
			repo.findAll().forEach(beer -> listBeer.add(beer));
			new ResponseEntity<List<BeerItem>>(listBeer, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}

}
