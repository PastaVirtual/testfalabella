package io.swagger.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.swagger.model.BeerItem;

@Repository
public interface BeerItemRepository extends CrudRepository<BeerItem, Integer> {

}
