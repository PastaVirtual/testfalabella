package io.swagger.api;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import io.swagger.model.BeerItem;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BeersApiControllerIntegrationTest {

	@Autowired
	private BeersApi api;

	@Test
	public void addBeersTest() throws Exception {
		BeerItem body = new BeerItem();
		ResponseEntity<Void> responseEntity = api.addBeers(body);
		assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
	}

	@Test
	public void searchBeerByIdTest() throws Exception {
		Integer beerID = 56;
		ResponseEntity<BeerItem> responseEntity = api.searchBeerById(beerID);
		assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
	}

	@Test
	public void searchBeersTest() throws Exception {
		ResponseEntity<List<BeerItem>> responseEntity = api.searchBeers();
		assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
	}

}
