package io.swagger.api;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-08-14T00:03:04.071Z[GMT]")
public class ApiException extends Exception {

	private static final long serialVersionUID = -2174637728997742359L;
	private int code;

	public ApiException(int code, String msg) {
		super(msg);
		this.setCode(code);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
